#include <iostream>
#include <iomanip>

int main() {
    int a = 1;
    int b = 5;
    double f = 3.14;
    char c = 'C';
    char d;

    std::cout << "a: valeur = " << a << ", adresse = " << std::hex << &a << std::endl;
    std::cout << "b: valeur = " << b << ", adresse = " << std::hex << &b << std::endl;
    std::cout << "f: valeur = " << f << ", adresse = " << std::hex << &f << std::endl;
    std::cout << "c: valeur = " << c << ", adresse = " << std::hex << &c << std::endl;
    std::cout << "d: adresse = " << std::hex << &d << std::endl; 


    d = 'D';
    std::cout << "d: valeur = " << d << ", adresse = " << std::hex << &d << std::endl;

    std::swap(a, b);
    std::cout << "Apr�s permutation de a et b: a = " << a << ", b = " << b << std::endl;

    double temp = f;
    f = static_cast<double>(a);
    a = static_cast<int>(temp);
    std::cout << "Apr�s permutation de a et f: a = " << a << ", f = " << f << std::endl;

    a = static_cast<int>(f);
    std::cout << "Apr�s affectation de f � a: f = " << f << ", a = " << a << std::endl;

}